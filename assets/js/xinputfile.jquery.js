// JavaScript jQuery Plugin xFader - Alexandre Mattos
(function(){

	//Definicao do nome do plugin e chamada com opcoes
	jQuery.fn.xInputFile = function(options){

		//Seta os valores default
		var defaults = {
			maxMbSize:5,
			types:['jpg','doc','gif','pdf'],
			disableRemoveBtn:false
		}

		var options = $.extend(defaults, options);

		//Inicializacao do plugin
		return this.each(function(){

			//Objeto
			var inputFileObject = jQuery(this);
			if ( !inputFileObject.parent().hasClass('fileBox') ){

				//	Função converte bytes em tamanhos em megabytes, gigabytes, etc.
				function bytesToSize(bytes) {
					var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
					if (bytes == 0) return 'n/a';
						var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
					if (i == 0) return bytes + ' ' + sizes[i];
						return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
				};

				//Cria o filebox
				var fb = inputFileObject.wrap( "<div class='fileBox'></div>" );

				var obj = inputFileObject.parent();

				var html = '<div class="file">';
				html += '<p>Nenhum arquivo selecionado</p>';
				html += '<a class="fileEvent" href="javascript:void(0);">Escolher arquivo</a>';
				if ( options.disableRemoveBtn== false ){
					html += '<a class="removeFile" href="javascript:void(0);">Remover</a>';
				}
				html += '</div>';



				var input = obj.find('input[type="file"]');

				// Em Megabytes
				var maxSize = options.maxMbSize * 1024 * 1024;

				obj.append(html);

				obj.find('.fileEvent').click(function(){
					input.click();
				});

				obj.find('.removeFile').click(function(){
					input.val("");
					jQuery(this).removeClass('active');
					obj.find('.fileEvent').show();
					file = "Nenhum arquivo selecionado";
					obj.find('.file > p').html(file);
				});

				input.change(function(){
					obj.find("p.msgErro").remove();

					// Se não tiver arquivo selecionado
					if ( jQuery(this).val() != "" ){
						var str = jQuery(this).val().split("\\");
						var fSize = jQuery(this)[0].files[0].size;

						// Pega o nome do arquivo
						if ( str == "" || str == undefined ){
							var file ="Nenhum arquivo selecionado";
							obj.find('.file > p').html(file);
						}else{
							var file = str[str.length-1];
							var typexpt = file.split(".");
							var type = typexpt[typexpt.length-1];
							var list = options.types;

							if ( list.indexOf(type.toLowerCase()) != -1 ){

								// Verifica o tamano do arquivo
								if ( fSize > maxSize ){
									var mensagem = file + ' ('+bytesToSize(fSize)+') ultrapassou o máximo permitido de ' + bytesToSize(maxSize);
									file = "Nenhum arquivo selecionado";
									obj.find('.file > p').html(file);
									obj.append('<p class="msgErro">'+mensagem+'</p>');
									obj.closest('form').trigger('reset');
								}else{
									obj.find('.file > p').html(file + ' ('+bytesToSize(fSize)+')' );
									obj.find('.removeFile').addClass('active');
									if ( options.disableRemoveBtn == false ){
										obj.find('.fileEvent').hide();
									}
								}
							}else{
								file = "Nenhum arquivo selecionado";
								obj.find('.file > p').html(file);
								input.val("");
								mensagem = "Somente arquivos (" + options.types.toString() + ") são permitidos";
								obj.append('<p class="msgErro">'+mensagem+'</p>');
							}
						}
					}else{
						file = "Nenhum arquivo selecionado";
						obj.find('.file > p').html(file);
					}
				});

			}

		}); //Fim do processo

	}

})(jQuery);


























